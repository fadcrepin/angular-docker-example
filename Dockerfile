FROM node:15.4.0-alpine as build
WORKDIR /usr/src/app
ENV PATH=${PATH}:./node_modules/.bin
ENV NODE_PATH=/usr/src/app/node_modules
ADD package.json ./
ADD package-lock.json ./
RUN npm ci
ADD . .
RUN ng build --prod

FROM nginx:1.19-alpine
COPY --from=build /usr/src/app/dist/meb /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]